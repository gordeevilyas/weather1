
public class Weather {
    String date;
    String temperature;
    String humidity;
    String speed;
    String direction;

    public Weather(String date, String temperature, String humidity, String speed, String direction) {
        this.date = date;
        this.temperature = temperature;
        this.humidity = humidity;
        this.speed = speed;
        this.direction = direction;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }
}
