import java.io.*;
import java.util.*;
import java.util.stream.Collectors;


public class WeatherMain {
    ArrayList<Weather> weatherArrayList = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        WeatherMain weatherMain = new WeatherMain();
        File weatherFile = new File("src/dataexport_20210320T064822.csv");
        weatherMain.makingStrListFromFile(weatherFile);
        weatherMain.weatherManagerMethod();

    }

    public void makingStrListFromFile(File file) {

        String str;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while ((str = bufferedReader.readLine()) != null) {
                String[] split = str.split(",");
                if (isRightPart(split))
                    weatherArrayList.add(new Weather(split[0], split[1], split[2], split[3], split[4]));
            }
            bufferedReader.close();
        } catch (IOException exc) {
            System.out.println("IO error!" + exc);
        }
    }

    public static boolean isRightPart(String[] splitArr) {
        String firstPart = splitArr[0];
        char symbol = firstPart.charAt(0);
        return symbol >= '0' & symbol <= '9';
    }

    public String findCommonWindDirection()
    {
        List<String> listOfDirections = weatherArrayList.stream().map((weather) ->
        {
            double windDirection = Double.parseDouble(weather.getDirection());
            if (windDirection >= 315 && windDirection < 45)
            {
                return "North";
            }
            else if (windDirection >= 45 && windDirection < 135)
            {
                return "East";
            }
            else if (windDirection >= 135 && windDirection < 225)
            {
                return "South";
            }
            else
                {
                return "West";
                }
        }).collect(Collectors.toList());
        TreeMap<String, Long> treeMap = new TreeMap<>();

        treeMap.put("North", listOfDirections.stream().filter(direction -> direction.equals("North")).count());
        treeMap.put("East", listOfDirections.stream().filter(direction -> direction.equals("East")).count());
        treeMap.put("South", listOfDirections.stream().filter(direction -> direction.equals("South")).count());
        treeMap.put("West", listOfDirections.stream().filter(direction -> direction.equals("West")).count());
        Collection<Long> values = treeMap.values();
        Long maxValue = values.stream().mapToLong(x -> x).max().getAsLong();
        Set<String> keySet = treeMap.keySet();

        return keySet.stream().filter(key -> treeMap.get(key).equals(maxValue)).findFirst().get();
    }

    public File weatherManagerMethod() throws IOException {
        String[] days = new String[3];
        double avgTemperature = 0;
        double avgSpeed = 0;
        double avgHumidity = 0;
        double maxTemp = 0;
        double minHum = 100000;
        double maxSpeed = 0;
        for (Weather temp : weatherArrayList) {
            avgTemperature += Double.parseDouble(temp.getTemperature()) / weatherArrayList.size();
            avgSpeed += Double.parseDouble(temp.getSpeed()) / weatherArrayList.size();
            avgHumidity += Double.parseDouble(temp.getHumidity()) / weatherArrayList.size();

        }
        for (Weather weather : weatherArrayList) {
            if (Double.parseDouble(weather.getTemperature()) > maxTemp) {
                maxTemp = Double.parseDouble(weather.getTemperature());
                days[0] = weather.getDate();
            }
            if (Double.parseDouble(weather.getHumidity()) < minHum) {
                minHum = Double.parseDouble(weather.getHumidity());
                days[1] = weather.getDate();
            }
            if (Double.parseDouble(weather.getSpeed()) > maxSpeed) {
                maxSpeed = Double.parseDouble(weather.getSpeed());
                days[2] = weather.getDate();
            }
        }


        File finalFile = new File("src/Final.txt");
        FileWriter writer = new FileWriter(finalFile);
        writer.write("Avg Temperature: " + avgTemperature + "\n" +
                "Avg Humidity: " + avgHumidity + "\n" +
                "Avg Speed: " + avgSpeed + "\n" +
                "Max temperature: " + maxTemp + " was " + days[0] + "\n" +
                "Min humidity: " + minHum + " was " + days[1] + "\n" +
                "Max speed: " + maxSpeed + " was " + days[2] + "\n" +
                "Windiest side: " + findCommonWindDirection());
        writer.close();
        return finalFile;
    }
}

